package ro.pub.cs.systems.eim.Colocviu1_245;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.provider.SyncStateContract;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.util.Vector;

public class Colocviu1_245MainActivity extends AppCompatActivity {

    private EditText next_term;
    private TextView all_terms;
    private Button add, compute;

    private Vector<Integer> nums;


    private ButtonClickListener buttonClickListener = new ButtonClickListener();
    private class ButtonClickListener implements View.OnClickListener {
        @Override
        public void onClick(View view) {

            switch(view.getId()) {
                case R.id.add:
                    if (next_term.getText().equals("")) {
                        return;
                    }
                    int number = Integer.valueOf(next_term.getText().toString());

                    if (nums.size() == 0){
                        all_terms.setText(String.valueOf(number));
                    }
                    else {
                        all_terms.setText(" + " + String.valueOf(number));
                    }

                    nums.add(number);
                    break;
                case R.id.compute:
                    Intent intent = new Intent(getApplicationContext(), Colocviu1_245SecondaryActivity.class);
//                    intent.putExtra();
                    break;
            }

        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        next_term = (EditText)findViewById(R.id.next_term);
        add = (Button) findViewById(R.id.add);

        all_terms = (TextView) findViewById(R.id.all_terms);
        compute = (Button)findViewById(R.id.compute);

        next_term.setText(String.valueOf(0));
        all_terms.setText(String.valueOf(0));

        add.setOnClickListener(buttonClickListener);
    }
}
